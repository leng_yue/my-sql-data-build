﻿using System.Collections.Generic;

namespace MySqlDataBuild
{
    public class DataEntity
    {
        public string ip, port, pwd, user;
        public int pathIndex = -1;
        public int exIndex = -1;
        public List<string> paths = new List<string>();
        public List<string> exPaths = new List<string>();
        public int namespaceIndex;
        public bool clearOldFiles, compatible, creatDbPath;
        public int incrementIdType = 4;
    }
}
