﻿using SqlBuildLib;
using System.Text;

namespace SqlBuildConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            if (args != null && args.Length > 0)
            {
                try
                {
                    var items = args[0].Split(',');
                    string dbName = items[0];
                    string ip = items[1];
                    string port = items[2];
                    string user = items[3];
                    string pwd = items[4];
                    string savePath = items[5];
                    int nameSpaceMode = int.Parse(items[6]);
                    string nameSpace = items[7];
                    bool createDbDirectory = bool.Parse(items[8]);
                    bool compatibleName = bool.Parse(items[9]);
                    string incrementIdType = items[10];
                    bool isCreateFile = bool.Parse(items[11]);
                    int version = int.Parse(items[12]);
                    App.version = version;
                    GenerateCodeHelper.MySqlBuild(dbName, ip, port, user, pwd, savePath, "", nameSpaceMode, nameSpace,
                        createDbDirectory, compatibleName, incrementIdType, isCreateFile);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("发生错误:" + ex);
                }
            }
        }
    }
}