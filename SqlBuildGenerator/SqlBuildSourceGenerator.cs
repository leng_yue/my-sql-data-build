﻿using System;
using System.Text;
using System.Diagnostics;
using Microsoft.CodeAnalysis;

namespace MySqlBuild
{
    [Generator]
    public class SqlBuildSourceGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
            //if (!Debugger.IsAttached) Debugger.Launch(); //调式时可以启用
        }

        public void Execute(GeneratorExecutionContext context)
        {
            foreach (var file in context.AdditionalFiles)
            {
                if (file.Path.EndsWith("SqlBuildConfig.conf"))
                {
                    var content = file.GetText().ToString();
                    string dbName = "";
                    string ip = "";
                    string port = "";
                    string user = "";
                    string pwd = "";
                    string savePath = "";
                    string nameSpaceMode = "";
                    string nameSpace = "";
                    string createDbDirectory = "";
                    string compatibleName = "";
                    string incrementIdType = "";
                    string isCreateFile = "";
                    string version = "";
                    string exePath = "";
                    var items = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in items)
                    {
                        var texts = item.Split('=');
                        if (texts.Length < 2)
                            continue;
                        var value = texts[1].Split('#')[0].Trim().TrimStart('"').TrimEnd('"');
                        switch (texts[0].Trim().ToLower())
                        {
                            case "dbname":
                                dbName = value;
                                break;
                            case "ip":
                                ip = value;
                                break;
                            case "port":
                                port = value;
                                break;
                            case "user":
                                user = value;
                                break;
                            case "pwd":
                                pwd = value;
                                break;
                            case "savepath":
                                savePath = value;
                                break;
                            case "namespacemode":
                                nameSpaceMode = value;
                                break;
                            case "namespace":
                                nameSpace = value;
                                break;
                            case "createdbdirectory":
                                createDbDirectory = value;
                                break;
                            case "compatiblename":
                                compatibleName = value;
                                break;
                            case "incrementidtype":
                                incrementIdType = value;
                                break;
                            case "iscreatefile":
                                isCreateFile = value;
                                break;
                            case "version":
                                version = value;
                                break;
                            case "exepath":
                                exePath = value;
                                break;
                        }
                    }
                    var startInfo = new ProcessStartInfo
                    {
                        FileName = exePath,
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        StandardOutputEncoding = Encoding.UTF8,
                        Arguments = $"{dbName},{ip},{port},{user},{pwd},{savePath},{nameSpaceMode},{nameSpace},{createDbDirectory},{compatibleName},{incrementIdType},{isCreateFile},{version}",
                    };
                    var sqlBuildProcess = new Process() { StartInfo = startInfo };
                    try
                    {
                        sqlBuildProcess.Start();
                    }
                    catch (Exception ex)
                    {
                        var diagnostic = Diagnostic.Create(new DiagnosticDescriptor("SqlBuildSourceGenerator", "", "SqlBuildConfig配置的exePath路径存在错误!", "", DiagnosticSeverity.Error, true), Location.None);
                        context.ReportDiagnostic(diagnostic);
                        diagnostic = Diagnostic.Create(new DiagnosticDescriptor("SqlBuildSourceGenerator", "", "发生错误:" + ex.ToString(), "", DiagnosticSeverity.Error, true), Location.None);
                        context.ReportDiagnostic(diagnostic);
                        return;
                    }
                    var text = sqlBuildProcess.StandardOutput.ReadToEnd();
                    if (text.StartsWith("发生错误"))
                    {
                        var diagnostic = Diagnostic.Create(new DiagnosticDescriptor("SqlBuildSourceGenerator", "", text, "", DiagnosticSeverity.Error, true), Location.None);
                        context.ReportDiagnostic(diagnostic);
                        return;
                    }
                    var codeTexts = text.Split(new string[] { "{end}" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var codeText in codeTexts)
                    {
                        items = codeText.Split(new string[] { "{split}" }, StringSplitOptions.RemoveEmptyEntries);
                        if (items.Length == 2)
                        {
                            items[0] = items[0].Trim();
                            items[1] = items[1].Trim();
                            context.AddSource(items[0], items[1]);
                        }
                    }
                    sqlBuildProcess.WaitForExit();
                }
            }
        }
    }
}