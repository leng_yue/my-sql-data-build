using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Data.SQLite;

namespace SqlBuildLib
{
    public class GenerateCodeHelper
    {
        public static string MySqlBuild(string dbName, string ip, string port, string user, string pwd, string savePath, string exPath,
            int nameSpaceMode, string nameSpace, bool createDbDirectory, bool compatibleName, string uniqueIdType, bool isCreateFile)
        {
            var assebly = Assembly.Load(App.version == 0 ? Properties.Resources.MySql_6_x_x : Properties.Resources.MySql_8_x_x);
            var type = assebly.GetType("MySql.Data.MySqlClient.MySqlConnection");
            var type1 = assebly.GetType("MySql.Data.MySqlClient.MySqlCommand");
            var type2 = assebly.GetType("MySql.Data.MySqlClient.MySqlConnectionStringBuilder");
            var builder = (DbConnectionStringBuilder)Activator.CreateInstance(type2);
            builder.Add("database", dbName);
            builder.Add("server", ip);
            builder.Add("port", uint.Parse(port));
            builder.Add("user id", user);
            builder.Add("password", pwd);
            builder.Add("characterset", "utf8mb4");
            builder.Add("pooling", true);
            builder.Add("compress", true);
            builder.Add("connectiontimeout", 60u);
            builder.Add("sslMode", "None");
            var connBuilderCode = $@"Database = ""{dbName}"",
            Server = ""{ip}"",
            Port = {port},
            UserID = ""{user}"",
            Password = ""{pwd}"",
            CharacterSet = ""utf8mb4"",
            Pooling = true,
            UseCompression = true,
            ConnectionTimeout = 60,
            {(App.version == 1 ? "AllowLoadLocalInfile = true" : "")}";
            var connStr = builder.ToString();
            var connect = (DbConnection)Activator.CreateInstance(type, new object[] { connStr });
            connect.Open();
            var cmd = (DbCommand)Activator.CreateInstance(type1);

            var queryTableAllcmdText = $"select table_name from information_schema.tables where table_schema='{dbName}'";

            return BuildNew(dbName, connect, cmd, nameSpaceMode, nameSpace, builder, savePath, exPath, createDbDirectory, compatibleName,
                queryTableAllcmdText, 0, "MySql.Data.MySqlClient", "MySqlParameter", "MySqlConnection", "MySqlCommand", "MySqlDataAdapter", "MySqlTransaction", "connection.Ping();//长时间没有连接后断开连接检查状态",
                $@"try
                {{
                    var fileName = PathHelper.Combine(batchLoaderPath, $""{{tableName}}{{Process.GetCurrentProcess().Id}}.txt""); //解决分布式本地测试共用一个文件导致的问题
                    using (var stream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {{
                        stream.SetLength(0);
                        var text = updateCmdText.ToString();
                        var bytes = Encoding.UTF8.GetBytes(text);
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }}
                    var bulkLoader = new MySqlBulkLoader(Connection)
                    {{
                        TableName = $""`{{tableName}}`"",
                        FieldTerminator = ""|"",
                        LineTerminator = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? ""\r\n"" : ""\n"",
                        NumberOfLinesToSkip = 0,
                        FileName = fileName,
                        EscapeCharacter = '\\',
                        Local = true,
                        CharacterSet = ""utf8mb4"",
                        ConflictOption = MySqlBulkLoaderConflictOption.Replace,
                    }};
                    var stopwatch = Stopwatch.StartNew();
                    var rowsAffected = bulkLoader.Load();
                    SubmitCount += rowsAffected;
                    stopwatch.Stop();
                    if (rowsAffected > 2000) NDebug.Log($""SQL批处理完成:{{rowsAffected}} 用时:{{stopwatch.Elapsed}}"");
                    result += rowsAffected;
                }}
                catch (Exception ex)
                {{
                    NDebug.LogError($""表{{tableName}}批量错误: 1.如果表字段更改则需要重新生成! 2.打开Navicat菜单工具->命令列界面,输入SHOW VARIABLES LIKE 'local_infile';后回车,看是否开启批量加载!如果没有则输入SET GLOBAL local_infile = ON;回车设置批量加载! 详细信息:"" + ex);
                    return -1;
                }}", uniqueIdType, connBuilderCode, isCreateFile, "Config.Database = name;", "return Config.Database;",
                @"var cmdType = typeof(MySqlCommand);
            nonQueryHandlerField = cmdType.GetField(""NonQueryHandler"");
            if (nonQueryHandlerField == null)
            {
                NDebug.LogError(""未注入MySql!, 如果要使用新优化的NonQueryAsync非查询方法，请在Main入口函数加上这段代码: MySQLHelper.Inject(\""mysql.dll的nuget路径\""); 第一次注入需要重新编译一下即可, 必须提前于GameDB.Init，否则可能会出现写入失败!"");
                NDebug.LogError(@""通常nuget路径在:@""""C:\Users\Administrator\.nuget\packages\mysql.data\8.x.x\lib\net7.0\MySql.Data.dll"""""");
                NDebug.LogError(@""1.如果是Core项目,请查看->项目解决方案/你的项目/依赖项/包/MySql.Data/编译时程序集/MySql.Data.dll的属性, 复制属性窗口的路径填入MySQLHelper.Inject方法参数!"");
                NDebug.LogError(@""2.如果是Framework项目,请查看->项目解决方案/你的项目/引用/MySql.Data.dll的属性, 复制属性窗口的路径填入MySQLHelper.Inject方法参数!"");
            }");
        }

        public static string SqliteBuild(string dbPath, string savePath,
            int nameSpaceMode, string nameSpace, bool createDbDirectory, bool compatibleName, string uniqueIdType, bool isCreateFile)
        {
            var assebly = typeof(SQLiteConnection).Assembly;
            var type = assebly.GetType("System.Data.SQLite.SQLiteConnection");
            var type1 = assebly.GetType("System.Data.SQLite.SQLiteCommand");
            var dbName = Path.GetFileNameWithoutExtension(dbPath);
            var builder = new SQLiteConnectionStringBuilder
            {
                DataSource = dbPath,
            };
            var connBuilderCode = $@"DataSource = @""{dbPath}""";
            var connStr = builder;
            var connect = (DbConnection)Activator.CreateInstance(type, new object[] { connStr.ToString() });
            connect.Open();
            var cmd = (DbCommand)Activator.CreateInstance(type1);

            var queryTableAllcmdText = $"SELECT name FROM sqlite_master where type='table' order by name";

            return BuildNew(dbName, connect, cmd, nameSpaceMode, nameSpace, connStr, savePath, "", createDbDirectory, compatibleName,
                queryTableAllcmdText, 1, "System.Data.SQLite", "SQLiteParameter", "SQLiteConnection", "SQLiteCommand", "SQLiteDataAdapter", "SQLiteTransaction", "",
                @"var stopwatch = Stopwatch.StartNew();
                var rowsAffected = ExecuteNonQuery(updateCmdText.ToString());
                stopwatch.Stop();
                if (rowsAffected > 2000) NDebug.Log($""SQL批处理完成:{rowsAffected} 用时:{stopwatch.Elapsed}"");
                if (rowsAffected <= 0)
                    return -1;
                result += rowsAffected;", uniqueIdType, connBuilderCode, isCreateFile, "", "return Path.GetFileNameWithoutExtension(Config.DataSource);", string.Empty);
        }

        private static string BuildNew(string dbName, DbConnection connect, DbCommand cmd, int nameSpaceMode, string nameSpace,
            DbConnectionStringBuilder connStr, string savePath, string exPath, bool createDbDirectory, bool compatibleName, string queryTableAllcmdText, int sqlType,
            string usingName, string sqlParameter, string sqlConnect, string sqlCmd, string dataAdapter, string sqlTransaction, string ping,
            string batching, string uniqueIdType, string connBuilderCode, bool isCreateFile, string setDatabaseName, string getDatabaseName, string inject)
        {
            var dbUpp = dbName.ToUpperFirst();
            var dbTable = new DataTable();
            cmd.CommandText = queryTableAllcmdText;
            cmd.Connection = connect;
            using (var sdr = cmd.ExecuteReader())
            {
                dbTable.Load(sdr);
            }
            var createStatement = string.Empty;
            if (sqlType == 0)
            {
                cmd.CommandText = $"SHOW CREATE DATABASE `{dbName}`";
                cmd.Connection = connect;
                using (var sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        createStatement = sdr.GetString(1);
                        createStatement = createStatement.Replace(dbName, "{dbName}");
                    }
                }
            }
            var tableNames = new List<StringEntiy>();
            foreach (DataRow row in dbTable.Rows)
            {
                var des = row[0].ToString();
                var des1 = des.ToUpperFirst();
                tableNames.Add(new StringEntiy(des, des1));
            }
            var rpcMaskDic = new Dictionary<string, Dictionary<string, string[]>>();
            var createTableSqls = new List<DataRow>();
            foreach (var tableName in tableNames)
            {
                var table = new DataTable();
                var table2 = new DataTable();
                if (sqlType == 0)
                    cmd.CommandText = $@"SELECT COLUMN_NAME columnName, COLUMN_TYPE dataType, IS_NULLABLE allowDBNull, COLUMN_KEY columnKey, CHARACTER_MAXIMUM_LENGTH maxLength, COLUMN_COMMENT caption FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '{dbName}' AND TABLE_NAME = '{tableName.source}' ORDER BY ORDINAL_POSITION;";
                else
                    cmd.CommandText = $@"PRAGMA table_info('{tableName.source}');";
                cmd.Connection = connect;
                using (var sdr = cmd.ExecuteReader())
                {
                    table.TableName = tableName.source;
                    while (sdr.Read())
                    {
                        string fieldName;
                        string fieldType;
                        string allowDBNull;
                        string columnKey;
                        string maxLengthStr;
                        string caption;
                        if (sqlType == 0)
                        {
                            fieldName = sdr["columnName"].ToString();
                            fieldType = sdr["dataType"].ToString();
                            allowDBNull = sdr["allowDBNull"].ToString();
                            columnKey = sdr["columnKey"].ToString();
                            maxLengthStr = sdr["maxLength"].ToString();
                            caption = sdr["caption"].ToString();
                            Type dataType = null;
                            if (fieldType == "tinyint")
                                dataType = typeof(sbyte);
                            else if (fieldType == "tinyint unsigned")
                                dataType = typeof(byte);
                            else if (fieldType == "smallint")
                                dataType = typeof(short);
                            else if (fieldType == "smallint unsigned")
                                dataType = typeof(ushort);
                            else if (fieldType == "int" | fieldType.StartsWith("int("))
                                dataType = typeof(int);
                            else if (fieldType == "int unsigned")
                                dataType = typeof(uint);
                            else if (fieldType == "bigint")
                                dataType = typeof(long);
                            else if (fieldType == "bigint unsigned")
                                dataType = typeof(ulong);
                            else if (fieldType == "guid")
                                dataType = typeof(Guid);
                            else if (fieldType == "smalldatetime" | fieldType == "date" | fieldType == "datetime")
                                dataType = typeof(DateTime);
                            else if (fieldType == "timestamp" | fieldType == "time")
                                dataType = typeof(TimeSpan);
                            else if (fieldType == "float" | fieldType.StartsWith("float("))
                                dataType = typeof(float);
                            else if (fieldType == "double" | fieldType.StartsWith("double("))
                                dataType = typeof(double);
                            else if (fieldType == "numeric" | fieldType == "smallmoney" | fieldType == "decimal" | fieldType == "money")
                                dataType = typeof(decimal);
                            else if (fieldType == "bit" | fieldType == "bool" | fieldType == "boolean" | fieldType.StartsWith("tinyint(1)") | fieldType.StartsWith("bit(1)"))
                                dataType = typeof(bool);
                            else if (fieldType == "image" | fieldType == "binary" | fieldType == "blob" | fieldType == "mediumblob" | fieldType == "longblob" | fieldType == "varbinary")
                                dataType = typeof(byte[]);
                            else if (fieldType.StartsWith("image(") | fieldType.StartsWith("binary(") | fieldType.StartsWith("blob(") | fieldType.StartsWith("mediumblob(") | fieldType.StartsWith("longblob(") | fieldType.StartsWith("varbinary(") | fieldType == "tinyblob")
                                dataType = typeof(byte[]);
                            else if (fieldType == "tinytext" | fieldType == "text" | fieldType == "longtext" | fieldType.StartsWith("varchar") | fieldType == "json")
                                dataType = typeof(string);
                            else
                                throw new Exception($"未知类型:{fieldType}, 请报告给作者处理!");
                            var dataColumn = new DataColumn
                            {
                                ColumnName = fieldName,
                                DataType = dataType,
                                AllowDBNull = allowDBNull == "YES",
                                Caption = caption,
                                Expression = maxLengthStr //maxLength属性只有是string才能设置, 所以用了Expression代替
                            };
                            table.Columns.Add(dataColumn);
                            if (columnKey == "PRI")
                                table.PrimaryKey = new DataColumn[] { dataColumn };
                        }
                        else
                        {
                            fieldName = sdr["name"].ToString();
                            fieldType = sdr["type"].ToString().ToLower();
                            allowDBNull = sdr["notnull"].ToString();
                            columnKey = sdr["pk"].ToString();
                            maxLengthStr = "";
                            caption = "";
                            Type dataType = null;
                            if (fieldType == "integer(1)")
                                dataType = typeof(byte);
                            else if (fieldType == "integer(2)")
                                dataType = typeof(short);
                            else if (fieldType == "integer(4)")
                                dataType = typeof(int);
                            else if (fieldType == "integer(8)" | fieldType == "integer")
                                dataType = typeof(long);
                            else if (fieldType == "real(4)")
                                dataType = typeof(float);
                            else if (fieldType == "real(8)" | fieldType == "real")
                                dataType = typeof(double);
                            else if (fieldType == "text")
                            {
                                dataType = typeof(string);
                                maxLengthStr = "1073741823";
                            }
                            else if (fieldType.StartsWith("text("))
                            {
                                dataType = typeof(string);
                                fieldType = fieldType.TrimStart("text(".ToCharArray());
                                fieldType = fieldType.TrimEnd(')');
                                maxLengthStr = fieldType;
                            }
                            else if (fieldType == "blob")
                            {
                                dataType = typeof(byte[]);
                                maxLengthStr = "1073741823";
                            }
                            else if (fieldType.StartsWith("blob("))
                            {
                                dataType = typeof(string);
                                fieldType = fieldType.TrimStart("blob(".ToCharArray());
                                fieldType = fieldType.TrimEnd(')');
                                maxLengthStr = fieldType;
                            }
                            else
                                throw new Exception($"未知类型:{fieldType}, 请报告给作者处理!");
                            var dataColumn = new DataColumn
                            {
                                ColumnName = fieldName,
                                DataType = dataType,
                                AllowDBNull = allowDBNull == "YES",
                                Caption = caption,
                                Expression = maxLengthStr //maxLength属性只有是string才能设置, 所以用了Expression代替
                            };
                            table.Columns.Add(dataColumn);
                            if (columnKey == "1")
                                table.PrimaryKey = new DataColumn[] { dataColumn };
                        }
                    }
                }
                tableName.ColumnsLength = table.Columns.Count;
                if (sqlType == 0)
                {
                    cmd.CommandText = $"SHOW CREATE TABLE `{tableName.source}`";
                    cmd.Connection = connect;
                    using (var sdr = cmd.ExecuteReader())
                    {
                        table2.Load(sdr);
                    }
                }
                else
                {
                    cmd.CommandText = $"SELECT name, sql FROM sqlite_master WHERE type = 'table' AND name = '{tableName.source}';";
                    cmd.Connection = connect;
                    using (var sdr = cmd.ExecuteReader())
                    {
                        table2.Load(sdr);
                    }
                }
                var codeText = Properties.Resources.tableTemplate;
                var codeTextEx = Properties.Resources.tableTemplateEx;
                if (nameSpaceMode == 1)
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {dbUpp}\r\n{{");
                    codeText = codeText.Replace("{NAMESPACE_END}", "}");

                    codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", $"namespace {dbUpp}\r\n{{");
                    codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "}");
                }
                else if (nameSpaceMode == 2)
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                    codeText = codeText.Replace("{NAMESPACE_END}", "}");

                    codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                    codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "}");
                }
                else
                {
                    codeText = codeText.Replace("{NAMESPACE_BEGIN}", "");
                    codeText = codeText.Replace("{NAMESPACE_END}", "");

                    codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", "");
                    codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "");
                }
                codeText = codeText.Replace("{TYPENAME}", $"{tableName.newString}Data");
                if (sqlType == 1)
                    codeText = codeText.Replace("bool isBulk = true", "bool isBulk = false");
                var pkeys = table.PrimaryKey;
                if (pkeys.Length == 0)
                    throw new Exception($"{table.TableName}表必须设置Key, 每个表必须设置一个Key");
                tableName.key = pkeys[0].ColumnName;
                codeText = codeText.Replace("{KETTYPE}", $"{GetCodeType(pkeys[0].DataType)}");
                codeText = codeText.Replace("{KETTYPETRUE}", $"{pkeys[0].DataType.FullName}");
                var colName1 = pkeys[0].ColumnName.ToLowerFirst();
                var colName2 = pkeys[0].ColumnName.ToUpperFirst();
                codeText = codeText.Replace("{KEYNAME1}", $"{colName1}");
                codeText = codeText.Replace("{KEYNAME2}", $"{colName2}");
                codeText = codeText.Replace("{TABLENAME}", $"{tableName.source}");
                codeText = codeText.Replace("{DBNAME}", $"{dbUpp}DB");
                codeText = codeText.Replace("{COUNT}", $"{table.Columns.Count}");
                codeText = codeText.Replace("{PARAMETER}", sqlParameter);
                codeText = codeText.Replace("{CLIENT}", $"{dbUpp}DBEvent");
                codeText = codeText.Replace("{DBNAME1}", $"{dbUpp}");
                codeText = codeText.Replace("{TABLENAME1}", tableName.newString);
                codeText = codeText.Replace("{RPCHASHTYPE}", $"{dbUpp}HashProto");
                codeText = codeText.Replace("{SYNCID}", $"{tableName.newString}Data_SyncID");

                codeTextEx = codeTextEx.Replace("{TYPENAME}", $"{tableName.newString}Data");
                codeTextEx = codeTextEx.Replace("{DBNAME}", $"{dbUpp}DB");

                var codeTexts = codeText.Split(new string[] { "[split]" }, 0);

                var dic = new Dictionary<string, string[]>();
                foreach (DataColumn item in table.Columns)
                {
                    var length = item.Expression;
                    if (string.IsNullOrEmpty(length)) length = "0";
                    dic[item.ColumnName] = new string[] { item.Caption, length };
                }

                foreach (DataRow item in table2.Rows)
                {
                    createTableSqls.Add(item);
                }

                codeTexts[0] = codeTexts[0].Replace("{KEYNOTE}", $"{dic[pkeys[0].ColumnName][0]}");
                codeTexts[0] = codeTexts[0].Replace("{USING}", usingName);
                //codeTexts[0] = codeTexts[0].Replace("{RPCHASHTYPE}", $"{dbUpp}HashProto");

                var sb_code = new StringBuilder(codeTexts[0]);
                var sb_no3 = new StringBuilder();
                var sb_no5 = new StringBuilder();
                var sb_no9 = new StringBuilder();
                var sb_no7 = new StringBuilder();
                var sb_no11 = new StringBuilder();
                var sb_no13 = new StringBuilder();
                var sb_no15 = new StringBuilder();

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    var cell = table.Columns[i];
                    if (char.IsUpper(cell.ColumnName[0]))
                    {
                        return $"表:{table.TableName}的列:{cell.ColumnName} 出错! 列名前缀不能使用大写!";
                    }
                    var isKey = cell == pkeys[0];

                    var cellName = cell.ColumnName;
                    var fieldName1 = cellName.ToLowerFirst();
                    var fieldName2 = cellName.ToUpperFirst();
                    var rpcenumName = (table.TableName + "_" + cell.ColumnName).ToUpperAll();

                    var indexCode = codeTexts[3].Replace("{INDEX}", $"{i}");
                    indexCode = indexCode.Replace("{INDEXNAME}", $"{fieldName1}");
                    indexCode = indexCode.Replace("{TEXTLENGTH}", $"{dic[cell.ColumnName][1]}");
                    sb_no3.Append(indexCode);

                    var indexCode1 = codeTexts[5].Replace("{INDEX}", $"{i}");
                    indexCode1 = indexCode1.Replace("{INDEXNAME}", $"{(isKey ? fieldName1 : $"{fieldName1}.Value")}");
                    sb_no5.Append(indexCode1);

                    var indexCode11 = codeTexts[9].Replace("{FIELDNAME1}", $"\"{fieldName1}\"");
                    indexCode11 = indexCode11.Replace("{INDEXNAME}", $"{(isKey ? fieldName1 : $"{fieldName1}.Value")}");
                    sb_no9.Append(indexCode11);

                    bool isArray = cell.DataType == typeof(byte[]);
                    var isAtk = !isArray & cell.DataType != typeof(bool) & cell.DataType != typeof(string) & cell.DataType != typeof(DateTime);
                    var indexCode2 = codeTexts[7].Replace("{INDEX}", $"{i}");
                    if (isKey)
                        indexCode2 = indexCode2.Replace("{ADDORCUTROW}", $"this.{fieldName1} = ({GetCodeType(cell.DataType)})value;");
                    else
                        //indexCode2 = indexCode2.Replace("{ADDORCUTROW}", $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value(({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1);");
                        indexCode2 = indexCode2.Replace("{ADDORCUTROW}", $"SetPropertyValue(this.{fieldName1}, ({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1, {dbUpp}HashProto.{rpcenumName});");
                    sb_no7.Append(indexCode2);

                    var indexCode22 = codeTexts[11].Replace("{FIELDNAME1}", $"\"{fieldName1}\"");
                    if (isKey)
                        indexCode22 = indexCode22.Replace("{ADDORCUTROW}", $"this.{fieldName1} = ({GetCodeType(cell.DataType)})value;");
                    else
                        //indexCode22 = indexCode22.Replace("{ADDORCUTROW}", $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value(({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1);");
                        indexCode22 = indexCode22.Replace("{ADDORCUTROW}", $"SetPropertyValue(this.{fieldName1}, ({GetCodeType(cell.DataType)})Convert.ChangeType(value, typeof({GetCodeType(cell.DataType)})), -1, {dbUpp}HashProto.{rpcenumName});");
                    sb_no11.Append(indexCode22);

                    var indexCode4 = codeTexts[13].Replace("{INDEX}", $"{i}");
                    indexCode4 = indexCode4.Replace("{FIELDTYPE}", $"{GetCodeType(cell.DataType)}");
                    indexCode4 = indexCode4.Replace("{FIELDTYPE1}", $"{GetCodeType1(cell.DataType)}Obs");
                    indexCode4 = indexCode4.Replace("{FIELDNAME1}", $"{fieldName1}");
                    //indexCode4 = indexCode4.Replace("{INIT}", isKey ? $"this.{fieldName1} = {fieldName1};" : $"Check{fieldName2}{(isArray ? "Bytes" : "")}Value({(isArray ? $"Convert.FromBase64String(Encoding.ASCII.GetString({fieldName1}))" : $"{fieldName1}")}, -1);");
                    indexCode4 = indexCode4.Replace("{INIT}", isKey ? $"this.{fieldName1} = {fieldName1};" : $"SetPropertyValue(this.{fieldName1}, {(isArray ? $"Convert.FromBase64String(Encoding.ASCII.GetString({fieldName1}))" : $"{fieldName1}")}, -1, {dbUpp}HashProto.{rpcenumName});");
                    sb_no13.Append(indexCode4);

                    sb_no15.Append($"{fieldName2}:{{{fieldName2}{(isArray ? "Bytes" : "")}}} ");

                    if (isKey)
                        continue;

                    if (!rpcMaskDic.TryGetValue(table.TableName, out var dict))
                        rpcMaskDic.Add(table.TableName, dict = new Dictionary<string, string[]>());
                    dict.Add(rpcenumName, dic[cell.ColumnName]);

                    var fieldCode = codeTexts[1].Replace("{FIELDTYPE}", $"{GetCodeType(cell.DataType)}");
                    fieldCode = fieldCode.Replace("{FIELDTYPE1}", $"{GetCodeType1(cell.DataType)}Obs");
                    fieldCode = fieldCode.Replace("{FIELDTYPETRUE}", $"{cell.DataType.FullName}");
                    fieldCode = fieldCode.Replace("{FIELDNAME1}", $"{fieldName1}");
                    fieldCode = fieldCode.Replace("{FIELDNAME2}", $"{fieldName2}{(isArray ? "Bytes" : "")}");
                    fieldCode = fieldCode.Replace("{KEYNAME1}", $"{colName1}");
                    fieldCode = fieldCode.Replace("{PUBLIC}", $"{(isArray ? "internal" : "public")}");
                    fieldCode = fieldCode.Replace("{INDEX}", i.ToString());

                    fieldCode = fieldCode.Replace("{VARSET}", isArray ? $"object bytes = {fieldName1}.Value;" : "");
                    fieldCode = fieldCode.Replace("{VARSET1}", (isKey ? fieldName1 : isArray ? $"bytes" : $"{fieldName1}.Value"));

                    fieldCode = fieldCode.Replace("{ISATK}", isAtk.ToString().ToLower());

                    fieldCode = fieldCode.Replace("{RPCHASHTYPE}", $"{dbUpp}HashProto");
                    fieldCode = fieldCode.Replace("{RPCHASHVALUE}", $"{rpcenumName}");
                    fieldCode = fieldCode.Replace("{NOTE}", $"{dic[cell.ColumnName][0]}");
                    fieldCode = fieldCode.Replace("{NOTE1}", $"{dic[cell.ColumnName][0]} --同步到数据库");
                    fieldCode = fieldCode.Replace("{NOTE2}", $"{dic[cell.ColumnName][0]} --同步带有Key字段的值到服务器Player对象上，需要处理");
                    fieldCode = fieldCode.Replace("{NOTE3}", $"{dic[cell.ColumnName][0]} --同步当前值到服务器Player对象上，需要处理");
                    fieldCode = fieldCode.Replace("{NOTE4}", $"{dic[cell.ColumnName][0]} --获得属性观察对象");

                    fieldCode = fieldCode.Replace("{JUDGE}", isArray ? "" : $"if (this.{fieldName1}.Value == value)\r\n                return;");

                    sb_code.Append(fieldCode);
                }

                codeTexts[15] = codeTexts[15].Replace("{TOSTRING}", sb_no15.ToString());

                sb_code.Append(codeTexts[2]);
                sb_code.Append(sb_no3);
                sb_code.Append(codeTexts[4]);
                sb_code.Append(sb_no5);
                sb_code.Append(codeTexts[6]);
                sb_code.Append(sb_no7);
                sb_code.Append(codeTexts[8]);
                sb_code.Append(sb_no9);
                sb_code.Append(codeTexts[10]);
                sb_code.Append(sb_no11);
                sb_code.Append(codeTexts[12]);
                sb_code.Append(sb_no13);
                sb_code.Append(codeTexts[14]);
                sb_code.Append(codeTexts[15]);

                if (isCreateFile)
                {
                    {
                        string path;
                        if (string.IsNullOrEmpty(savePath))
                            path = AppDomain.CurrentDomain.BaseDirectory;
                        else
                            path = savePath + "\\";
                        if (createDbDirectory)
                            path += dbUpp + "DB";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                        path += "\\" + (compatibleName ? dbUpp + "_" : "") + tableName.newString + "Data.cs";
                        File.WriteAllText(path, sb_code.ToString());
                    }
                    {
                        if (string.IsNullOrEmpty(exPath))
                            continue;
                        var path = exPath + "\\";
                        if (createDbDirectory)
                            path += dbUpp + "DBEx";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                        path += "\\" + (compatibleName ? dbUpp + "_" : "") + tableName.newString + "DataEx.cs";
                        if (File.Exists(path)) //文件已存在就不能覆盖
                            continue;
                        File.WriteAllText(path, codeTextEx);
                    }
                }
                else
                {
                    var codeText2 = (compatibleName ? dbUpp + "_" : "") + tableName.newString + "Data.cs" + "{split}" + sb_code.ToString();
                    Console.WriteLine(codeText2 + "{end}");
                }
            }
            return BuildDBNew(dbUpp, tableNames, rpcMaskDic, createTableSqls, createStatement, connStr, nameSpaceMode, nameSpace, savePath, exPath,
                usingName, sqlConnect, sqlCmd, dataAdapter, sqlTransaction, ping, batching, uniqueIdType, connBuilderCode, isCreateFile, setDatabaseName, getDatabaseName, inject);
        }

        private static string GetCodeType(Type type)
        {
            var code = Type.GetTypeCode(type);
            if (code != TypeCode.Object)
                return code.ToString();
            return type.FullName;
        }

        private static string GetCodeType1(Type type)
        {
            var code = Type.GetTypeCode(type);
            if (code != TypeCode.Object)
                return code.ToString();
            if (type == typeof(byte[]))
                return "Bytes";
            if (type == typeof(TimeSpan))
                return "TimeSpan";
            return type.FullName;
        }

        private static string BuildDBNew(string db, List<StringEntiy> tableNames, Dictionary<string, Dictionary<string, string[]>> rpcMaskDic,
            List<DataRow> createTableSqls, string createStatement, DbConnectionStringBuilder connStr, int nameSpaceMode,
            string nameSpace, string savePath, string exPath, string usingName, string sqlConnect, string sqlCmd, string dataAdapter, string sqlTransaction, string ping,
            string batching, string uniqueIdType, string connBuilderCode, bool isCreateFile, string setDatabaseName, string getDatabaseName, string inject)
        {
            var sb1 = new StringBuilder();
            sb1.AppendLine($"public enum {db}HashProto : uint");
            sb1.AppendLine("{");
            int index = 4096;
            foreach (var item in rpcMaskDic)
            {
                bool f = false;
                foreach (var item1 in item.Value)
                {
                    sb1.AppendLine($"    /// <summary>{item1.Value[0]}</summary>");
                    sb1.AppendLine($"    {item1.Key}{(f ? "" : $" = {index}")},");
                    f = true;
                }
                index += 1000;
            }
            sb1.AppendLine("}");

            var codeText15 = $@"NAMESPACE_BEGIN
    public class {db}DBEvent
    {{
        private static Net.Client.ClientBase client;
        /// <summary>
        /// 设置同步到服务器的客户端对象, 如果不设置, 则默认是ClientBase.Instance对象
        /// </summary>
        public static Net.Client.ClientBase Client
        {{
            get
            {{
                if (client == null)
                    client = Net.Client.ClientBase.Instance;
                return client;
            }}
            set => client = value;
        }}

        /// <summary>
        /// 当服务器属性同步给客户端, 如果需要同步属性到客户端, 需要监听此事件, 并且调用发送给客户端
        /// 参数1: 要发送给哪个客户端
        /// 参数2: cmd
        /// 参数3: protocol
        /// 参数4: pars
        /// </summary>
        public static System.Action<Net.Server.NetPlayer, Net.Share.NetCmd, uint, object[]> OnSyncProperty;

        /// <summary>
        /// 当实体行属性更改时触发
        /// 参数1: 实体行对象接口
        /// 参数1: 哪个属性被修改
        /// 参数2: 数据的唯一id
        /// 参数3: 数据的更改值
        /// </summary>
        public static System.Action<Net.Share.IDataRow, {db}HashProto, object, object> OnValueChanged;

        SYNC_KEY
    }}
NAMESPACE_END";

            var uniqueIdTypeCode = $@"#if SERVER
NAMESPACE_BEGIN
    public partial class {db}UniqueIdType
    {{
        {{FIELD}}
    }}
NAMESPACE_END
#endif
";

            var db1 = db + "DB";
            var codeText = Properties.Resources.databaseTemplate;
            var codeTextEx = Properties.Resources.databaseTemplateEx;
            if (nameSpaceMode == 1)
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {db}\r\n{{");
                codeText = codeText.Replace("{NAMESPACE_END}", "}");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", $"namespace {db}\r\n{{");
                codeText15 = codeText15.Replace("NAMESPACE_END", "}");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_BEGIN", $"namespace {db}\r\n{{");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_END", "}");

                codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", $"namespace {db}\r\n{{");
                codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "}");
            }
            else if (nameSpaceMode == 2)
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                codeText = codeText.Replace("{NAMESPACE_END}", "}");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", $"namespace {nameSpace}\r\n{{");
                codeText15 = codeText15.Replace("NAMESPACE_END", "}");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_BEGIN", $"namespace {nameSpace}\r\n{{");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_END", "}");

                codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", $"namespace {nameSpace}\r\n{{");
                codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "}");
            }
            else
            {
                codeText = codeText.Replace("{NAMESPACE_BEGIN}", "");
                codeText = codeText.Replace("{NAMESPACE_END}", "");
                codeText15 = codeText15.Replace("NAMESPACE_BEGIN", "");
                codeText15 = codeText15.Replace("NAMESPACE_END", "");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_BEGIN", "");
                uniqueIdTypeCode = uniqueIdTypeCode.Replace("NAMESPACE_END", "");

                codeTextEx = codeTextEx.Replace("{NAMESPACE_BEGIN}", "");
                codeTextEx = codeTextEx.Replace("{NAMESPACE_END}", "");
            }
            codeText = codeText.Replace("{DBNAME}", $"{db1}");
            codeText = codeText.Replace("{DBNAME1}", $"{db}");
            codeText = codeText.Replace("{USING}", usingName);
            codeText = codeText.Replace("{CONNECT}", sqlConnect);
            codeText = codeText.Replace("{COMMAND}", sqlCmd);
            codeText = codeText.Replace("{DATAADAPTER}", dataAdapter);
            codeText = codeText.Replace("{TRANSACTION}", sqlTransaction);
            codeText = codeText.Replace("{PING}", ping);
            codeText = codeText.Replace("{DATABASENAME}", db);
            codeText = codeText.Replace("{BATCHING}", batching);
            codeText = codeText.Replace("{UNIQUEIDTYPE}", uniqueIdType);
            codeText = codeText.Replace("{CONNECTIONBUILDERTYPE}", connStr.GetType().Name);
            codeText = codeText.Replace("{CONNECTIONBUILDERFIELD}", connBuilderCode);
            codeText = codeText.Replace("{SETDBNAME}", setDatabaseName);
            codeText = codeText.Replace("{GETDBNAME}", getDatabaseName);
            codeText = codeText.Replace("{INJECT}", inject);

            codeTextEx = codeTextEx.Replace("{DBNAME}", $"{db1}");

            var codeTexts = codeText.Split(new string[] { "[split]" }, 0);

            var sb = new StringBuilder(codeTexts[0]);
            var sb_no1 = new StringBuilder();
            var sb_no2 = new StringBuilder();
            var sb_no3 = new StringBuilder();
            var sb_no4 = new StringBuilder();
            var sb3 = new StringBuilder();
            var sb_no5 = new StringBuilder();

            var sb_no6 = new StringBuilder();

            for (int i = 0; i < tableNames.Count; i++)
            {
                var tableName = tableNames[i];
                var indexCode = codeTexts[1].Replace("{TABLENAME}", $"{tableName.source}");
                indexCode = indexCode.Replace("{TABLENAME1}", $"{tableName.newString}");
                sb_no1.Append(indexCode);
                sb3.Append($"/// <summary>{tableName.newString}Data类对象属性同步id索引</summary>\r\n\t\tpublic static int {tableName.newString}Data_SyncID = 0;\r\n\t\t");
                sb_no2.AppendLine($"{(sb_no2.Length == 0 ? "" : "            ")}var {tableName.source}UniqueId = ExecuteScalar<{uniqueIdType}>(@\"SELECT MAX({tableName.key}) FROM `{tableName.source}`;\");");
                sb_no2.AppendLine($"{(sb_no2.Length == 0 ? "" : "            ")}uniqueIdMap[{db}UniqueIdType.{tableName.newString}] = new UniqueIdGenerator(useMachineId, machineId, machineIdBits, (long){tableName.source}UniqueId);");
                sb_no6.AppendLine($"{(sb_no6.Length == 0 ? "" : "        ")}public const short {tableName.newString} = {i + 1};");
                sb_no3.AppendLine($"dataRowHandler[typeof({tableName.newString}Data)] = new(\"{tableName.key}\");");
                sb_no4.AppendLine($"queryTypes[typeof({tableName.newString}Data)] = new();");
            }

            codeTexts[2] = codeTexts[2].Replace("{INCREMENTIDMAP}", sb_no2.ToString());
            codeTexts[2] = codeTexts[2].Replace("{TABLEHANDLER}", sb_no3.ToString());
            codeTexts[2] = codeTexts[2].Replace("{TABLEQUERYHANDLER}", sb_no4.ToString());
            uniqueIdTypeCode = uniqueIdTypeCode.Replace("{FIELD}", sb_no6.ToString());

            for (int i = 0; i < createTableSqls.Count; i++)
            {
                var item = createTableSqls[i];
                var text = codeTexts[5].Replace("{TABLENAME}", item.ItemArray[0].ToString());
                text = text.Replace("{DATABASENAME}", db);
                text = text.Replace("{CREATETABLESQL}", item.ItemArray[1].ToString().Replace("\"", "'"));
                text = text.Replace("{TABLECOUNT}", tableNames[i].ColumnsLength.ToString());
                sb_no5.Append(text);
            }

            codeTexts[4] = codeTexts[4].Replace("{CREATEDATABASESQL}", createStatement);

            sb.Append(sb_no1);
            sb.Append(codeTexts[2]);
            sb.Append(codeTexts[3]);
            sb.Append(codeTexts[4]);
            sb.Append(sb_no5);
            sb.Append(codeTexts[6]);

            codeText15 = codeText15.Replace("SYNC_KEY", sb3.ToString());

            sb.Replace("\r\n", "\n").Replace("\n", "\r\n");
            sb1.Replace("\r\n", "\n").Replace("\n", "\r\n");
            codeText15 = codeText15.Replace("\r\n", "\n").Replace("\n", "\r\n");
            uniqueIdTypeCode = uniqueIdTypeCode.Replace("\r\n", "\n").Replace("\n", "\r\n");

            if (isCreateFile)
            {
                string path, path1, path2, path3;
                if (string.IsNullOrEmpty(savePath))
                {
                    path = AppDomain.CurrentDomain.BaseDirectory + db1 + ".cs";
                    path1 = AppDomain.CurrentDomain.BaseDirectory + $"{db}HashProto.cs";
                    path2 = AppDomain.CurrentDomain.BaseDirectory + $"{db}DBEvent.cs";
                    path3 = AppDomain.CurrentDomain.BaseDirectory + $"{db}UniqueIdType.cs";
                }
                else
                {
                    path = savePath + "/" + db1 + ".cs";
                    path1 = savePath + "/" + $"{db}HashProto.cs";
                    path2 = savePath + "/" + $"{db}DBEvent.cs";
                    path3 = savePath + "/" + $"{db}UniqueIdType.cs";
                }
                File.WriteAllText(path, sb.ToString());
                File.WriteAllText(path1, sb1.ToString());
                File.WriteAllText(path2, codeText15);
                File.WriteAllText(path3, uniqueIdTypeCode);
                if (!string.IsNullOrEmpty(exPath))
                {
                    path = exPath + "/" + db1 + "Ex.cs";
                    if (!File.Exists(path))
                        File.WriteAllText(path, codeTextEx);
                }
            }
            else
            {
                Console.WriteLine($"{db1}.cs{{split}}{sb}{{end}}");
                Console.WriteLine($"{db}HashProto.cs{{split}}{sb1}{{end}}");
                Console.WriteLine($"{db}DBEvent.cs{{split}}{codeText15}{{end}}");
                Console.WriteLine($"{db}UniqueIdType.cs{{split}}{uniqueIdTypeCode}{{end}}");
            }
            return "生成成功!";
        }

    }
}
